def somme_article(liste_art):
    """Permet de faire la somme des prix articles

    Args:
        liste_art (liste d'entrée): Liste d'article

    Returns:
        Float: Retourne le résultat de la somme de tous les prix des articles
    """    
    somme = 0
    for i in range(len(liste_art)):
        somme += liste_art[i][2]
    return somme

print(somme_article(([152,"Chaussures",37.5],[145,"Veste",87.2],[147,"T-Shirt",25.3],[165,"Bonnet",11.0])))

def test_somme_article():
    assert somme_article(([152,"Chaussures",37.5],[145,"Veste",87.2],[147,"T-Shirt",25.3],[165,"Bonnet",11.0]))==161.0
    assert somme_article(([152,"Gant",37.5],[123,"Veste",20.0],[178,"Bonnet",135.0],[163,"Pantalon",35.9]))==228.4
    assert somme_article(([142,"Gant",32.3],[138,"Veste",29.0],[183,"Bonnet",28.0],[163,"Pantalon",35]))==124.3



def pluscherarticle(list_art):
    """[summary]

    Args:
        list_art ([type]): [description]

    Returns:
        [type]: [description]
    """    
    gprix=0
    plusarticle=None
    for i in range(len(list_art)):
        if list_art[i][2]>gprix:
            gprix=list_art[i][2]
            plusarticle=list_art[i]
    return plusarticle

print(pluscherarticle([(152,"Chaussures",37.5),(145,"Veste",38.5),(147,"T-Shirt",25.3),(165,"Bonnet",11.0)]))

def test_pluscherarticle():
    assert pluscherarticle([(152,"Chaussures",37.5), (145,"Veste",87.2), (147,"T-Shirt",25.3), (165,"Bonnet",11.0)]) == (145,"Veste",87.2)
    assert pluscherarticle([(152,"Gant",37.5), (123,"Veste",20.0), (178,"Bonnet",135.0), (163,"Pantalon",35.9)]) == (178,"Bonnet",135.0)
    assert pluscherarticle(([142,"Gant",32.3],[138,"Veste",32.3],[183,"Bonnet",28.0],[163,"Pantalon",31.9]))==[142,"Gant",32.3]


def prod_50(list_art):
    article_50euros=[]
    for i in range(len(list_art)):
        if list_art[i][2]<50:
            article_50euros.append(list_art[i][1])
    return article_50euros

print(prod_50([(152,"Chaussures",37.5),(145,"Veste",38.5),(147,"T-Shirt",52),(165,"Bonnet",11.0)]))

def test_pluscherarticle():
    assert prod_50([(152,"Chaussures",37.5), (145,"Veste",87.2), (147,"T-Shirt",25.3), (165,"Bonnet",11.0)]) == ["Chaussures","T-Shirt","Bonnet"]
    assert prod_50([(152,"Gant",37.5), (123,"Veste",20.0), (178,"Bonnet",135.0), (163,"Pantalon",35.9)]) == ["Gant","Veste","Pantalon"]
    assert prod_50([(142,"Gant",50.3),(138,"Veste",32.3),(183,"Bonnet",55.0),(163,"Pantalon",60.9)])== ["Veste"]


def trouvearticleref(list_art,ref):
    for i in range(len(list_art)):
        list_art[i][0]=ref

print(trouvearticleref([(152,"Chaussures",37.5),(145,"Veste",38.5),(147,"T-Shirt",52),(165,"Bonnet",11.0)]))






