#Exercice 1.1
def moyennenbpositifs(liste_nombre):
    """Cette fonction permet de faire la moyenne des nombres positifs d'une liste de nombre donnée en entrée

    Args:
        liste_nombre (liste d'entrée): Liste de nombre

    Returns:
        float: Retourne la moyenne des nombre positif
    """    
    somme=0 #contient la somme tout tout les nombres énumérés jusqu'ici
    i=0 #contient l'indice des nombres énumérés jusqu'ici . Cela fait aussi office de compteur
    while  i < len(liste_nombre):
        if liste_nombre[i]<0:
            return 0
        somme+=liste_nombre[i]
        i+=1
    return somme/i

print(moyennenbpositifs([2,1,-2,2]))

def test_moyennenbpositifs():
    assert moyennenbpositifs ([2,4,4,2,2])==2.8
    assert moyennenbpositifs ([2,1,-2,2])==0
    assert moyennenbpositifs ([-2,1,1,3])==0
    assert moyennenbpositifs ([2,8,5,8,-4])==0



#Liste pour l'Exercice 1.2
liste_pers1=[
        ("Mathieu",18),  ("Guillin",20), ("Carole",37), ("Michel",61), 
        ("Corine",69), ("Bernard",73)
            ]

liste_pers2=[
        ("Mathieu",73),  ("Guillin",69), ("Carole",61), ("Michel",37), 
        ("Corine",20), ("Bernard",18)
            ]

liste_pers3=[
        ("Mathieu",73),  ("Guillin",69), ("Carole",61), ("Michel",37), 
        ("Corine",20), ("Bernard",21)
            ]

liste_pers4=[
        ("Mathieu",73),  ("Guillin",69), ("Carole",71), ("Michel",37), 
        ("Corine",20), ("Bernard",18)
            ]
#Exercice 1.2
def trieagedecroissant(liste_pers):
    """Cette fonction permet de savoir si une liste de personne est triée dans l'ordre décroissant en fonction des ages.

    Args:
        liste_pers (tuple d'entrée): (nom_pers,age_pers)

    Returns:
        Bool: Retourne Vrai si elle est décroissante et faux si elle est croissante
    """    
    i=1 #contient l'indice des nombres énumérés jusqu'ici
    verif=True #Contient un booléen pour être retourné par la suite
    while i < len(liste_pers):
        if liste_pers[i][1]>liste_pers[i-1][1]:
            verif=False
        i+=1
    return verif

print(trieagedecroissant(liste_pers3))

def test_trieagedecroissant():
    assert trieagedecroissant(liste_pers1)==False
    assert trieagedecroissant(liste_pers2)==True
    assert trieagedecroissant(liste_pers3)==False
    assert trieagedecroissant(liste_pers4)==False