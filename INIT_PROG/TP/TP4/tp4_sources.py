from typing import Mapping


def plus_long_plateau(chaine):
    """recherche la longueur du plus grand plateau d'une chaine

    Args:
        chaine (str): une chaine de caractères

    Returns:
        int: la longueur de la plus grande suite de lettres consécutives égales
    """    
    lg_max=0 # longueur du plus grand plateau déjà trouvé
    lg_actuelle=0 #longueur du plateau actuel
    for i in range (len(chaine)):
        if chaine[i]==chaine[i-1]: # si la lettre actuelle est égale à la précédente
            lg_actuelle+=1
        else: # si la lettre actuelle est différente de la précédente
            if lg_actuelle>lg_max:
                lg_max=lg_actuelle
            lg_actuelle=1
    if lg_actuelle>lg_max: #cas du dernier plateau
        lg_max=lg_actuelle
    return lg_max

print(plus_long_plateau("zzzztttre"))


def test_plus_long_plateau():
    assert plus_long_plateau("zzzztttrerryyt") == 4
    assert plus_long_plateau("yeerrrtzelllll") == 5

# --------------------------------------
# Exemple de villes avec leur population
# --------------------------------------
def villelppeuple(liste_villes,population):
    """La fonction permet de connaitre le nom de la ville la plus peuplé de la liste

    Args:
        liste_villes (liste): Liste de ville
        population (liste): population de la ville

    Returns:
        Villelpg: str Doit retournée la ville la plus peuplée
    """    
    populationlpg=0
    villelpg=0

    for i in range (len(population)):
        if population[i]>populationlpg:
            populationlpg=population[i]
            villelpg=liste_villes[i]
    return villelpg

print(villelppeuple(["Blois","Bourges","Chartes","Châteauroux","Dreux","Joué-lès-Tours","Olivet","Orleans","Tours","Vierzon"],[45871, 64668,  38426, 43442, 30664, 38250, 22168, 116238, 136463,  25725]))
    

def test_villelppeuple():
    assert villelppeuple(["Blois","Bourges","Chartes","Châteauroux","Dreux","Joué-lès-Tours","Olivet","Orleans","Tours","Vierzon"],[45871, 64668,  38426, 43442, 30664, 38250, 22168, 116238, 136463,  25725]) == "Tours"
    assert villelppeuple(["Hière","Angers","Mer","Florence"],[4378,43298,-12503,8932])=="Angers"
    assert villelppeuple(["Blois","Bourges","Chartes","Châteauroux","Dreux","Joué-lès-Tours","Olivet","Orleans","Tours","Vierzon"],[45871, 64668,  138426, 43442, 30664, 38250, 22168, 116238, 136463,  25725]) == "Chartes"






def caractèretoint(chaine):
    """La fonction permet de passer d'un nombre en chaine de caractère à un nombre 

    Args:
        chaine (liste): liste d'un nombre en chaine de caractère 

    Returns:
        int: Un Nombre
    """
    res = 0
    for i in range(len(chaine)):
        if chaine [i] == 0:
            return res
        if chaine [i] == "":
            return res
        else:
            if chaine [i] =="1":
                res=res*10+1
            elif chaine [i] =="2":
                res=res*10+2
            elif chaine [i] =="3":
                res=res*10+3
            elif chaine [i] =="4":
                res=res*10+4
            elif chaine [i] =="5":
                res=res*10+5
            elif chaine [i] =="6":
                res=res*10+6
            elif chaine [i] =="7":
                res=res*10+7
            elif chaine [i] =="8":
                res=res*10+8
            elif chaine [i] =="9":
                res=res*10+9
            elif chaine [i] =="0":
                res=res*10+0
    return res

def test_caractèretoint():
    assert caractèretoint("145")==145
    assert caractèretoint("103928")==103928
    assert caractèretoint("-2918")==2918






def commenceparlalettre(liste_mot,lettre):
    """Fonction qui permet de retrouver les mots qui commencent par une certaine lettre dans
une liste de mots

    Args:
        liste_mot (liste d'entrée): Une liste de mot
        lettre (lettre d'entrée): Lettre à laquelle on veux que le mot commence

    Returns:
        str: Retourne une liste de mot qui commence par la lettre du paramètre lettre 
    """    
    liste=[]
    for i in range(len(liste_mot)):
        if liste_mot[i] != "":
            if lettre==liste_mot[i][0]:
                liste.append(liste_mot[i])
    return liste
            




print(commenceparlalettre(["salut","hello","hallo","ciao","hola"],"a"))

def test_commenceparlalettre():
    assert commenceparlalettre(["salut","hello","hallo","ciao","hola"],"h")==["hello","hallo","hola"]
    assert commenceparlalettre(["bonjour","bienvenue","bravo","bouillon","bouteille"],"b")==["bonjour","bienvenue","bravo","bouillon","bouteille"]
    assert commenceparlalettre(["juste","loire","fine","portant","loup","erreur"],"a")==[]

def decoupemot(phrase):
    """Cette fonction permet de découper les mot

    Args:
        phrase (phrase d'entrée):

    Returns:
        str: Retourne la liste de mot de la phrase du paramètre d'entrée
    """
    mot_phrase=""
    nv_phrase=[] #list
    for i in range(len(phrase)):
        if phrase[i].isalpha():
            mot_phrase+=phrase[i]
        else:
            if len(mot_phrase)!=0: # si la longueur du mot est différent de chaine vide
                nv_phrase.append(mot_phrase)
            mot_phrase=""
    return nv_phrase

            
print(decoupemot("Cela fait déjà 28 jours! 28 jours à l’IUT’O! Cool!!"))


def test_decoupemot():
    assert decoupemot("Cela fait déjà 28 jours! 28 jours à l’IUT’O! Cool!!")==["Cela","fait","déjà","jours","jours","à","l","IUT","O","Cool"]
    assert decoupemot("Juste prix le 12 juin 2021")==["Juste","prix","le","juin"]
    assert decoupemot("La plus grande personne du monde mesure 2 metrès 25")==["La","plus","grande","personne","du","monde","mesure","metrès"]

