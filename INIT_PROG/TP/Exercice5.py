def algo1(a,b,c,d):
    """Permet de trouver le plus petit nombre de la liste

    Args:
        a (int): correspond à 15
        b (int): correspond au nombre 23
        c (int): correspond au nombre 7
        d (int): correspond au nombre 58
        (Dans ce cas là !!!)

    Returns:
        int: trouve le minimum des quatres nombres a,b,c,d
    """    
    if a<b:
        res=a
    else: #b<=a
        res=b
    # res est le min entre a et b
    if c<res:
        res=c
    # res est le min entre a,b et c
    if d<res:
        res=d
    # res est le min entre a,b,c et d
    return res

def test_algo1():
    assert algo1(15,23,7,58)==7
    assert algo1(14,10,45,38)

#programme principal
print(algo1(15,23,7,58))
print(algo1(14,10,45,38))




def algo2(mot):
    """indique si les voyelles sont majoritaires

    Args:
        mot (str): un mot en minuscules

    Returns:
        bool: True si il y a plus de voyelle que de consonnes dans le mot False sinon
    """    
    res=0
    for lettre in mot:
        if lettre in 'aeiouy':
            res+=1
        else:
            res-=1
        return res>0

def test_algo2():
    assert algo2("bonjour")==False
    assert algo2("tableau")==True
    assert algo2("tableau")==False

#programme principal
print(algo2("bonjour"))
print(algo2("phrase"))
#print(algo1(15,23,7,58))

