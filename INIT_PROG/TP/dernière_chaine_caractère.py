def dr_voyelle ( mots ) :
    """Cette fonction permet de connaitre la dernière voyelle d'un mot

    Args:
        mot (str): chaine de caractère

    Returns:
        str: dernière voyelle
    """    
    resultat = None
    # la variable resultat contient la dernière voyelle d'une mot énuméré
    # elem permet de savoir si il y a un caractère
    for elem in mots :
        if elem in 'eioyua':
            resultat=elem
    return resultat

print(dr_voyelle("bonjour"))

def test_voyelle():
    assert dr_voyelle("bonjour")=="u"
    assert dr_voyelle ("Bienvenue")=="e"
    assert dr_voyelle("buongiorno")=="o"
    assert dr_voyelle("Dernièrement")=="e"
    assert dr_voyelle("fltr")==None