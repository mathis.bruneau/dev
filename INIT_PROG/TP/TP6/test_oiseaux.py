import oiseaux
# --------------------------------------
# FONCTIONS
# --------------------------------------
liste_oiseaux=[
        ("Merle","Turtidé"), ("Mésange","Passereau"), ("Moineau","Passereau"), 
        ("Pic vert","Picidae"), ("Pie","Corvidé"), ("Pinson","Passereau"),
        ("Tourterelle","Colombidé"), ("Rouge-gorge","Passereau")
        ]
def test_recherche_oiseau():
    assert oiseaux.recherche_oiseau("Merle",liste_oiseaux)==("Merle","Turtidé")
    assert oiseaux.recherche_oiseau("Moineau",liste_oiseaux)==("Moineau","Passereau")
    assert oiseaux.recherche_oiseau("Pie",liste_oiseaux)==("Pie","Corvidé")

def test_recherche_par_famille():
    assert oiseaux.recherche_par_famille("Passereau",liste_oiseaux)==[('Mésange', 'Passereau'), ('Moineau', 'Passereau'), ('Pinson', 'Passereau'), ('Rouge-gorge', 'Passereau')]


def test_oiseau_le_plus_observe():
    assert oiseaux.oiseau_le_plus_observe(oiseaux.observations1)=="Moineau"
    assert oiseaux.oiseau_le_plus_observe(oiseaux.observations2)=="Tourterelle"
    assert oiseaux.oiseau_le_plus_observe(oiseaux.observations3)=="Mésange"
    assert oiseaux.oiseau_le_plus_observe([])==None

def test_est_liste_observations():
    assert oiseaux.est_liste_observations(oiseaux.observations1)==False
    assert oiseaux.est_liste_observations(oiseaux.observations2)==False
    assert oiseaux.est_liste_observations(oiseaux.observations4)==True



def test_max_observations():
    assert oiseaux.max_observations(oiseaux.observations1)==5
    assert oiseaux.max_observations(oiseaux.observations2)==5
    assert oiseaux.max_observations(oiseaux.observations3)==4
    assert oiseaux.max_observations(oiseaux.observations6)==None


def test_moyenne_oiseaux_observes():
    assert oiseaux.moyenne_oiseaux_observes(oiseaux.observations2)==2.5
    assert oiseaux.moyenne_oiseaux_observes(oiseaux.observations3)==2.8

def test_total_famille():
    assert oiseaux.total_famille(...)==...


def test_construire_liste_observations():
    assert oiseaux.construire_liste_observations(...)==...

def test_creer_ligne_sup():
    assert oiseaux.creer_ligne_sup(...)==...

def test_creer_ligne_noms_oiseaux():
    assert oiseaux.creer_ligne_noms_oiseaux(...)==...



