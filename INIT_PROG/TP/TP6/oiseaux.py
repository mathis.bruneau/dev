# --------------------------------------
# DONNEES
# --------------------------------------

# exemple de liste d'oiseaux observables
oiseaux=[
        ("Merle","Turtidé"), ("Mésange","Passereau"), ("Moineau","Passereau"), 
        ("Pic vert","Picidae"), ("Pie","Corvidé"), ("Pinson","Passereau"),
        ("Tourterelle","Colombidé"), ("Rouge-gorge","Passereau")
        ]
# exemples de listes de comptage ces listes ont la même longueur que oiseaux
comptage1=[2,0,5,1,2,0,5,3]
comptage2=[2,1,3,0,0,3,5,1]
comptage3=[0,4,0,3,2,1,4,2]

# exemples de listes d'observations. Notez que chaque liste correspond à la liste de comptage de
# même numéro
observations1=[
        ("Merle",2),  ("Moineau",5), ("Pic vert",1), ("Pie",2), 
        ("Tourterelle",5), ("Rouge-gorge",3)
            ]

observations2=[
        ("Merle",2), ("Mésange",1), ("Moineau",3), 
        ("Pinson",3),("Tourterelle",5), ("Rouge-gorge",1)
            ]

observations3=[
        ("Mésange",4),("Pic vert",2), ("Pie",2), ("Pinson",1),
        ("Tourterelle",4)
            ]
observations4=[
        ("Mésange",2),("Pic vert",4), ("Pie",4), ("Pinson",4),
        ("Tourterelle",4)
            ]
observations5=[
        ("Mésange",1),("Pic vert",4), ("Pie",3), ("Pinson",9),
        ("Eourterelle",5), ("Rouge-gorge",4)
            ]
observations6=[]
# --------------------------------------
# FONCTIONS
# --------------------------------------

def oiseau_le_plus_observe(liste_observations):
    """ recherche le nom de l'oiseau le plus observé de la liste (si il y en a plusieur on donne le 1er trouve)

    Args:
        liste_observations (list): une liste de tuples (nom_oiseau,nb_observes)

    Returns:
        str: l'oiseau le plus observé (None si la liste est vide)
    """
    if len(liste_observations)==0:
        return None
    oiseau_max=("",0)
    for i in range(len(liste_observations)):
        if liste_observations[i][1]>oiseau_max[1]:
            oiseau_max=liste_observations[i]
    return oiseau_max[0]
print(oiseau_le_plus_observe(observations1))
    


#--------------------------------------
# PROGRAMME PRINCIPAL
#--------------------------------------

#afficher_graphique_observation(construire_liste_observations(oiseaux,comptage3))
#comptage=saisie_observations(oiseaux)
#afficher_graphique_observation(comptage)
#afficher_observations(comptage,oiseaux)

def recherche_oiseau(nom,liste_oiseaux):
    """Permet de retrouver la famille d'un oiseaux

    Args:
        oiseaux (liste d'entrée) (str): Liste d'oiseaux
        nom (nom oiseaux): 

    Returns:
        str: retourne les caractéristiques de l'oiseaux
    """    
    for i in range(len(liste_oiseaux)):
        if liste_oiseaux[i][0] == nom :
            return liste_oiseaux[i]
    return None

def recherche_par_famille(nfamille,liste_oiseaux):
    """Permet de retrouver tous les oiseaux d’une même famille dans une liste
d’oiseaux

    Args:
        nfamille (str):
        liste_oiseaux (liste d'entrée):

    Returns:
        list: retourne la liste des nom qui correspond à la famille
    """    
    list_famille=[]
    for i in range(len(liste_oiseaux)):
        if liste_oiseaux[i][1]==nfamille:
            list_famille.append(liste_oiseaux[i])
    return list_famille
print(recherche_par_famille("Passereau",oiseaux))


def est_liste_observations(liste_observations):
    """fonction qui vérifie qu’une liste est bien une liste d’observations

    Args:
        liste_observations (liste d'entrée):

    Returns:
        Bool: Si elle respecte les conditions, True sinon False
    """    
    if liste_observations[0][1]==0:
        return False
    for i in range(1,len(liste_observations)):
        if liste_observations[i][1]==0 or liste_observations[i][0]<liste_observations[i-1][0]:
            return False
    return True
print(est_liste_observations(observations4))

def max_observations(liste_observations):
    """Fonction qui donne le plus grand nombre de spécimens observés dans une liste
d’observations

    Args:
        liste_observations ([type]): [description]

    Returns:
        int: Retourne le nombre de specimens observée
    """    
    PlusGrandSpecimens= 0
    if len(liste_observations)==0:
        return None
    for i in range(len(liste_observations)):
        if liste_observations[i][1]>PlusGrandSpecimens:
            PlusGrandSpecimens=liste_observations[i][1]
    return PlusGrandSpecimens
print(max_observations(observations1))

def moyenne_oiseaux_observes(liste_observations):
    """Fonction qui calcule le nombre moyen de spécimens observés dans une liste d’obser-
vations

    Args:
        liste_observations (liste d'entrée):

    Returns:
        float : Retourne la moyenne du nombre de specimens
    """    
    cpt_spe=0
    somme_spe=0
    for i in range(len(liste_observations)):
        somme_spe += liste_observations[i][1]
        cpt_spe+=1
    return somme_spe/cpt_spe
print(moyenne_oiseaux_observes(observations2))

def total_famille(liste_observations,liste_oiseaux,nom):
    nb_total_spe=0
    familleoiseaux=recherche_par_famille(liste_oiseaux,nom)
    for i in range(len(liste_observations)):
        if liste_oiseaux[i][0]==familleoiseaux[i][0]:
            liste_observations[i][1]+=nb_total_spe
    return nb_total_spe



