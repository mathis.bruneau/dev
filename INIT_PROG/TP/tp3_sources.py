# exercice 2

def plusDePairs(entree):
    """vérifier qu'il y a plus de nombre paire que de nombre impaire donnée en paramètre

    Args:
        entree (liste d'entrée): liste d'entiers

    Returns:
        Bool: True si il y a plus de nombre paire et false sinon
    """
    nb_paire=0
    nb_impaire=0
    # au début de chaque tour de boucle, nb_paire contient le nombre d'élément paires observés jusqu'ici
    # au début de chaque tour de boucle, nb_impaire contient le nombre d'élément impaires observés jusqu'ici
    for elem in entree:
        if elem%2==0:
            nb_paire+=1
        else:
            nb_impaire+=1
    return nb_paire>=nb_impaire


def test_plusDePairs():
    assert plusDePairs([25,9,14,6,2,4])==True
    assert plusDePairs([23,32,35,21,11,39])==False
    assert plusDePairs([32,34,12,3,9,67])==True
    assert plusDePairs([12,4,6,7,8,10])==True

print(plusDePairs([-4,5,-11,-56,5,-11]))
    

# exercice 3

def min_sup(liste_nombres,valeur):
    """trouve le plus petit nombre d'une liste supérieur à une certaine valeur

    Args:
        liste_nombres (list): la liste de nombres
        valeur (int ou float): la valeur limite du minimum recherché

    Returns:
        int ou float: le plus petit nombre de la liste supérieur à valeur
    """
    res=None
    # au début de chaque tour de boucle res est le plus petit élément déjà énuméré
    # supérieur à valeur
    for elem in liste_nombres:
        if res==None:
            if elem>valeur:
                res=elem

        else :
            if elem>valeur and elem<res:
                res=elem
    return res

print(min_sup([-2,-5,2,9.8,-8.1,7],0))



def test_min_sup():
    assert min_sup([8,12,7,3,9,2,1,4,9],5)==7
    assert min_sup([-2,-5,2,9.8,-8.1,7],0)==2
    assert min_sup([5,7,6,5,7,3],10)==None
    assert min_sup([],5)==None

# exercice 4
def nb_mots ( phrase ) :
    """Fonction qui compte le nombre de mots d'une phrase

    Args:
        phrase (str): une phrase dont les mots sont séparés par des espaces (éventuellement plusieurs)

    Returns:
        int: le nombre de mots de la phrase
    """    
    resultat =0
    c1 = ' '
    # au début de chaque tour de boucle
    # c1 vaut le caractère précedent de la liste du mot
    # c2 vaut le caractère de la liste du mot en cours
    # resultat vaut le nombre de mot
    for c2 in phrase :
        if c1 == ' ' and c2 != ' ':
            resultat = resultat +1
        c1 = c2
    return resultat

print(nb_mots(" ce  test ne  marche pas "))

def test_nb_mots():
    assert nb_mots("bonjour, il fait beau")==4
    assert nb_mots("houla!     je    mets beaucoup   d'  espaces    ")==6
    assert nb_mots(" ce  test ne  marche pas ")==5
    assert nb_mots("")==0 #celui ci non plus