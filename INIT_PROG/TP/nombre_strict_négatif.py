def propstricnégatif(liste):
    """[summary]

    Returns:
        [type]: [description]
    """
    nb_neg=0
    nb=0
    if liste == []:
        res = None
    else:
        for elem in liste:
            if elem<0:
                nb_neg+=1
            nb=nb+1
        res =nb_neg/nb
    return res


def test_prostricnégatif():
    assert propstricnégatif ([4,-2,8,2,-2,-7]) == 0.5
    assert propstricnégatif ([]) == None
    assert propstricnégatif ([-12,-1,-9,-2,-6]) == 1


test_prostricnégatif()