from typing import List


def mystere(liste,valeur):
    """[summary]

    Args:
        liste (liste d'entrée): 
        valeur (liste d'entrée): 

    Returns:
        int: Cette fonction revoie l'indice de la 4ème occurrence d'une valeur dans une liste
    """
    xxx=0 #contient le nb d'éléments enumérés
    yyy=0 #contient le nb d'occurence de valeur parmi les éléments énumérés
    for elem in liste:
        if elem==valeur:
            yyy+=1
            if yyy>3:
                return xxx
        xxx+=1
    return None

mystere([12,15,-3,12,28,12,15,12,28],12)

def IndChiffre(liste,valeur):
    nb_occurence=0
    for i in range(len(liste)):
        if liste[i]==valeur:
            nb_occurence+=1
            if nb_occurence>3:
                return i
    return None


#Exercice 2

def indpremiercar(liste_de_caractère):
    for i in range(len(liste_de_caractère)):
        if liste_de_caractère[i] in "123456789":
            return i
    return None

def test_indpremiercar():
    assert indpremiercar("on est le 30/09/2021")==10
    assert indpremiercar("Il y a 32 ans")==7


#Exercice 3

liste_villes=["Blois","Bourges","Chartres","Châteauroux","Dreux","Joué-lès-Tours","Olivet","Orléans","Tours","Vierzon"]
population=[45871, 64668,  38426, 43442, 30664, 38250, 22168, 116238, 136463,  25725]

def popville(liste_villes,population,Nom_ville):
    """Permet de retrouver la population de la ville

    Args:
        liste_villes (liste d'entrée)
        population (liste d'entrée)
        Nom_ville (str)

    Returns:
        int : retourne la population de la ville par rapport à la liste d'entrée
    """
    for i in range(len(liste_villes)):
        if liste_villes[i]==Nom_ville:
            return population[i] 
    return None

#Exercice 3:

def listeordrecroissant(liste):
    nbprec=0
    for i in range(len(liste)):
        if nbprec<liste[i]:
            nbprec=liste[i]
        else:
            return False
    return True

def test_listeordrecroissant():
    assert listeordrecroissant([1,4,1,2,3,4])==False
    assert listeordrecroissant([1,2,3,4,5,6])==True
    assert listeordrecroissant([1,4,7,9,17,29,45])==True


def Sommeseuilnb(liste_nombre,seuil):
    """fonction qui indique si la somme des éléments d’une liste de nombres depasse le seuil donnée en paramètre

    Args:
        liste_nombre (liste d'entrée): [description]
        seuil (int): [description]

    Returns:
        Bool: Retourne si la somme dépasse ou non le seuil
    """    
    sommenb=0
    for i in range(len(liste_nombre)):
        sommenb+=liste_nombre[i]
        if sommenb>seuil:
            return True
    return False

def test_Sommeseuilnb():
    assert Sommeseuilnb([1,3,5],7)==True
    assert Sommeseuilnb([3,8,19,21],8)==True
    assert Sommeseuilnb([1,9,3,6],45)==False
    assert Sommeseuilnb([1,7,30,2],41)==False

def verifemail(email):
    """Cette fonction permet de vérifier si une adresse mail est correcte

    Args:
        email (listye d'entrée): une adresse mail

    Returns:
        Bool: Retourne si l'adresse mail est correct ou non
    """    
    arobase=0 #contient le nombre d'arobase présente dans une adresse mail
    verif=False #Contient une booléens qui permet à la fin de déterminer si l'adresse mail est correct ou non
    if len(email)==0 or email[-1]=="." or email[-1]=="@":
        return False
    for i in range(len(email)):
        if email[i]==" ":
            return False
        if email[i]=="@":
            arobase+=1
        if arobase>1:
            return False
        if email[i]=="." and arobase==1:
            verif=True
    return verif

def test_verifemail():
    assert verifemail("math@gmail.com")==True
    assert verifemail("")==False
    assert verifemail("math@gmailcom")==False
    assert verifemail("math.gmail.com")==False
    assert verifemail("math@@gmail.com")==False
    assert verifemail("math@gmail.com.")==False
    assert verifemail("math @gmail.com")==False
    assert verifemail("@math@gmail.com")==False


            


# --------------------------------------
# Exemple de villes avec leur population
# --------------------------------------
liste_villes=["Blois","Bourges","Chartres","Châteauroux","Dreux","Joué-lès-Tours","Olivet","Orléans","Tours","Vierzon"]
population=[45871, 64668,  38426, 43442, 30664, 38250, 22168, 116238, 136463,  25725]



def meilleurscores(joueurs,scores,nomjoueurs):
    topscorejoueurs=0
    nomjoueursaffi=0
    for i in range (len(joueurs)):
        if nomjoueurs==joueurs[i]:
            nomjoueursaffi+=1
            if scores[i]>topscorejoueurs:
                topscorejoueurs=scores[i]
    if nomjoueursaffi==0:
        return None
    return topscorejoueurs
#---------------------------------------
# Exemple de scores
#---------------------------------------
scores=[352100,325410,312785,220199,127853]
nomjoueurs=['Batman','Robin','Batman','Joker','Batman']

print(meilleurscores('Batman',scores,nomjoueurs))
def test_meilleurscores():
    assert meilleurscores ('Batman', scores=[352100,325410,312785,220199,127853],nomjoueurs=['Batman','Robin','Batman','Joker','Batman'])==352100