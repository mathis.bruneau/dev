def meilleurscores(nomjoueurs,scores,joueurs):
    """Cette fonction permet de connaitre le meilleur score d’un joueur dont le nom sera donné en
paramètre

    Args:
        nomjoueurs (nom du joueurs en entrée):
        scores (liste d'entrée):
        joueurs (liste d'entrée): 

    Returns:
        int: Retourne le meilleur score du joueurs selectionner en parametre
    """    
    topscorejoueurs=0 #contient le meilleur score du joueur
    nomjoueursaffi=0 #contient le 
    for i in range (len(joueurs)):
        if joueurs[i]==nomjoueurs:
            nomjoueursaffi+=1
            if scores[i]>topscorejoueurs:
                topscorejoueurs=scores[i]
    if nomjoueursaffi==0:
        return None
    return topscorejoueurs
#---------------------------------------
# Exemple de scores
#---------------------------------------
scores=[352100,325410,312785,220199,127853]
joueurs=['Batman','Robin','Batman','Joker','Batman']

print(meilleurscores('Batman',scores,joueurs))
def test_meilleurscores():
    assert meilleurscores ('Batman', scores=[352100,325410,312785,220199,127853], joueurs=['Batman','Robin','Batman','Joker','Batman'])==352100
    assert meilleurscores ('Robin', scores=[352100,325410,312785,220199,127853], joueurs=['Batman','Robin','Batman','Joker','Batman'])==325410
    assert meilleurscores ('', scores=[352100,325410,312785,220199,127853], joueurs=['Batman','Robin','Batman','Joker','Batman'])==None

#Exercice 4.2
def ordredecroissantscores(scores):
    """Cette fonction permet de vérifié que les scores se trouve dans l'ordre décroissant

    Args:
        scores (liste d'entrée): Des scores

    Returns:
        Bool: Retourne si les scores sont bien dans l'ordre décroissant ou non
    """    
    # Aucun variable n'est necessaire. pas besoin de stocker de valeur
    for i in range(1,len(scores)):
        if scores[i]>scores[i-1]:
            return False
    return True

print(ordredecroissantscores([352100,325410,312785,220199,127853]))
def test_ordrecroissantscores():
    assert ordredecroissantscores([352100,325410,312785,220199,127853])==True
    assert ordredecroissantscores([352100,355410,312785,220199,127853])==False
    assert ordredecroissantscores([352100,355410,312785,220199,367853])==False
    assert ordredecroissantscores([352100,325410,332785,220199,367853])==False

#Exercice 4.3
def foisoujoueursapparait(nomjoueurs,scores,joueurs):
    """Cette fonction permet de déterminer combien de fois le joueurs est dans la liste des meilleurs scores

    Args:
        nomjoueurs (nom du joueurs en entrée)
        scores (liste d'entrée):
        joueurs (liste d'entrée): 

    Returns:
        int: Retourne le nombre de fois ou le joueurs est dans les meilleurs scores
    """    
    cpt_joueurs=0 #Contient le nombre de fois ou apparait le joueurs dans les meilleurs scores définit dans le parametre
    if nomjoueurs=="": 
        return None
    for i in range(len(joueurs)):
        if joueurs[i]==nomjoueurs:
            cpt_joueurs+=1
    return cpt_joueurs

def test_foisoujoueursapparait():
    assert foisoujoueursapparait('Batman', scores=[352100,325410,312785,220199,127853], joueurs=['Batman','Robin','Batman','Joker','Batman'])==3
    assert foisoujoueursapparait('Robin' , scores=[352100,325410,312785,220199,127853], joueurs=['Batman','Robin','Batman','Joker','Batman'])==1
    assert foisoujoueursapparait('Rosa', scores=[352100,325410,312785,220199,127853], joueurs=['Batman','Robin','Batman','Joker','Batman'])==0
    assert foisoujoueursapparait('' , scores=[352100,325410,312785,220199,127853], joueurs=['Batman','Robin','Batman','Joker','Batman'])==None