def somme_nb_pair(entree):
    """ Fait la somme de tous les nombre paire qu'il detecte dans une liste de nombre

    Args:
        entree (liste d'entrée): 

    Returns:
        Int: Valeur entière au retour de la fonction
    """

    sm_paire=0
    # sm_paire permet de faire la somme de la liste des nombres paires enuméré jusqu'ici
    for elem in entree:
        if elem%2==0:
            sm_paire=sm_paire+elem
    return sm_paire

def test_somme_nb_pair():
    assert somme_nb_pair([2,3,8,12,7,5])==22
    assert somme_nb_pair([-3,-4,2,-6,-5])==-8
    assert somme_nb_pair([5,7,9,-6,-3])==-6
    assert somme_nb_pair([3,8,18,13,-11])==26