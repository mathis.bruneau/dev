def qualif_jo(sexe,record_perso,nb_victoires,est_champion):
    """Indique si une personne est qualifiée au JO ou non

    Args:
        sexe (str): "H" pour Homme et "F" pour femme
        record_perso (float): temps en seconde du record personnel de la personne
        nb_victores (int): le nombre de victoire sur la saison
        est_champion (bool): True si la personne est championne du monde

    Returns:
        bool: True si la personne est qualifiée et False sinon
    """    
    res = False
    #je ne traite que les cas True
    if est_champion:
        res = True
    else : # non champion
        if nb_victoires >= 3:
            if sexe == 'F':
                if record_perso <= 15:
                    res = True
            else : # c'est un homme non champion
                if record_perso <= 12:
                    res = True
    return res

def test_qualif_jo():
    assert qualif_jo('H',11,2,False)==False
    assert qualif_jo('F',16,1,True)==True

print(qualif_jo('F',16,11,False))