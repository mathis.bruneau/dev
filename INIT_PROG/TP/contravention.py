def contravention(vitesse, limite):
    """Permet de derminer qu'elle contravention 

    Args:
        vitesse (float): [description]
        limite (int): limitation de vitesse
    Returns: Tuple (int,int,int)(amende, points, suspension)
    """ 
    amende = 0
    points = 0
    suspension = 0
    depassement = vitesse - limite
    if depassement > 0:
        if depassement <= 20 :
            points = 1
            if limite <= 50 :
                amende = 135
            else : # limite > 50
                amende = 68
        else : # depassement > 20
            if depassement <= 30: #Entre 20 et 30 km/h
                points = 2
                amende = 135
            else : # depassement > 30
                suspension = 3
                if depassement <= 40 : # 30 < depassement <= 40
                    points = 3
                    amende = 135
                else : # depassement > 40
                    if depassement <= 50 : # 40 < depassement <= 50 
                        points = 4
                        amende = 135
                    else : # depassement > 50
                        points = 6
                        amende = 1500
                    


    return (amende, points, suspension)

def test_contravention():
    assert contravention(49,50) == (0,0,0)
    assert contravention(60,50) == (135,1,0)
    assert contravention(150,60) == (1500,6,3)